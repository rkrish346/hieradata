# Puppet Manifest to run log archive script

class shoretel_common::audit_service (

  $enable_audit_service = hiera('shoretel_common::enable_audit_service', false),
  $auditd_conf_log_file = '/var/log/audit/audit.log',

){

  if ( $enable_audit_service) {
      package { 'audit':
        ensure => present,
        notify => Service['auditd']
      }

      package { 'audit-libs':
        ensure  => present,
        require => Package['audit'],
        notify  => Service['auditd']
      }

      file { '/etc/audit/auditd.conf':
        ensure  => file,
        mode    => '0755',
        owner   => 'root',
        group   => 'root',
        source  => 'puppet:///modules/shoretel_common/auditd/auditd.conf',
        notify  => Service['auditd'],
        require => [Package['audit'], Package['audit-libs']]
      }

      file { '/etc/audit/rules.d/audit.rules':
        ensure  => present,
        mode    => '0755',
        owner   => 'root',
        group   => 'root',
        source  => 'puppet:///modules/shoretel_common/auditd/audit_common.rules',
        notify  => Service['auditd'],
        require => [Package['audit'], Package['audit-libs']]
      }

      service { 'auditd':
        ensure    => true,
        enable    => true,
        subscribe => [Package['audit','audit-libs'], File['/etc/audit/rules.d/audit.rules'], File['/etc/audit/auditd.conf']],
        require   => [Package['audit','audit-libs'], File['/etc/audit/rules.d/audit.rules'], File['/etc/audit/auditd.conf']],
        path      => '/sbin/service',
        provider  => 'redhat'
      }
    }
  }
